"""calculdistance URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoView

from calculdistance.core.soap import calcul_distance_service, app, CalculDistanceService
from calculdistance.core.rest import currency

urlpatterns = [

    # REST
    path('currency/', currency),

    # SOAP
    path('calcul_distance/', DjangoView.as_view(
        services=[CalculDistanceService], tns='trouvetontrain.distances.calcul.soap',
        in_protocol=Soap11(validator='lxml'), out_protocol=Soap11(),
        cache_wsdl=False))
]
