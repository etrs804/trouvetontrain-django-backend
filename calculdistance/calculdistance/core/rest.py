from django.http import JsonResponse, HttpResponseBadRequest
import json

# Prix au kilomètre en euros
base = 0.50

# Taux devise par rapport à l'euro
taux = {
    "EUR": 1,
    "USD": 1.09,
    "GBP": 0.87,
    "JPY": 117.59,
    "AUD": 1.71,
    "CHF": 1.06,
    "CAD": 1.52,
    "PLN": 4.56,
}


def currency(request):
    
    # Récupère la data
    data = json.loads(request.body.decode('utf-8'))
    km = data['km']
    
    if data['currency'] in taux:
        devise = base * taux[data['currency']]
    else:
        return HttpResponseBadRequest("Currency not supported.")
    
    try:
        km = float(data['km'])
    except ValueError:
        return HttpResponseBadRequest("Invalid type for 'km'. Must be float.")
    
    # Calcul le prix du voyage estimé
    result = devise * km

    # Génère la réponse
    return JsonResponse({'result': result})
