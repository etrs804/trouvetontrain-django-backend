from django.views.decorators.csrf import csrf_exempt

from spyne.server.django import DjangoApplication
from spyne.model.primitive import Double
from spyne.service import Service
from spyne.protocol.soap import Soap11
from spyne.application import Application
from spyne.decorator import rpc

from math import sin, cos, sqrt, atan2, radians

# Classe métier attaché au serveur Web
class CalculDistanceService(Service):

    # Ici on décrit la logique, on spécifie seulement que la méthode sera distante (@rpc)
    @rpc(Double, Double, Double, Double, _returns=Double)
    def calcul_distance(ctx, from_longitude, from_latitude, to_longitude, to_latitude):

        # Rayon de la Terre (en Km)
        R = 6378.137

        # Convertion des latitudes et logitudes en radians
        from_longitude = radians(from_longitude)
        from_latitude = radians(from_latitude)

        to_longitude = radians(to_longitude)
        to_latitude = radians(to_latitude)
        
        # Relation fondamentale de trigonométriesphérique (delta entre from à to)
        delta_longitude = to_longitude - from_longitude
        delta_latitude = to_latitude - from_latitude

        # Distance angulaire en radians entre From et To avec le rayon de courbure local
        a = sin(delta_latitude / 2)**2 + cos(from_latitude) * cos(to_latitude) * sin(delta_longitude / 2)**2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        # Convertion de la distance en Km
        distance = R * c

        return distance


app = Application([CalculDistanceService],
    'trouvetontrain.distances.calcul.soap',
    in_protocol=Soap11(validator='lxml'),
    out_protocol=Soap11(),
)

calcul_distance_service = csrf_exempt(DjangoApplication(app))
