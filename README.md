# TrouveTonTrain - Backend
[![Build status](https://brassoud.visualstudio.com/trouvetontrain-django-backend/_apis/build/status/trouvetontrain-django-backend-%20CI)](https://brassoud.visualstudio.com/trouvetontrain-django-backend/_build/latest?definitionId=2)


Ce dépôt contient l'ensemble du code pour la partie Backend du projet TrouveTonTrain disponible ici : https://trouvetontrain.azurewebsites.net/


## Architecture

* SOAP : Service SOAP utilisé pour calculer la distance d'une gare à l'autre : 
  * URL : https://trouvetontrain-django-soap.azurewebsites.net/calcul_distance/
  * Paramètres (type double) : (longitude1, latitude1, longitude2, latitude2)
  * Retour (double) : distance estimée du trajet
* REST : Service utilisé pour calculer le coût estimé du trajet avec une devise donnée : 
  * URL : POST https://trouvetontrain-django-soap.azurewebsites.net/currency/
  *  Paramètres (body json) : km (double), currency (chaîne représentant la devise)
     *  Exemple : ```{ "km": 73.0, "currency": "EUR" }```
  * Retour (double) : prix estimé du trajet


## CI/CD

Chaque release sur la branche master est automatiquement déployée grâce aux pipelines Microsoft Azure.


## Build

Ce projet a été réalisé avec Django 3. Cloner le dépôt puis exécuter `python manage.py runserver` pour déployer le serveur de développement en local. Puis rendez-vous sur `http://localhost:8000/` pour effectuer vos tests.


## Métadonnées

Xavier BRASSOUD (@XavierBrassoud) 2020 - Projet réalisé dans un cadre d'étude en Master 1 TRI à l'université Savoie Mont-Blanc - ETRS804 WebServices (David TELISSON)
